import { createStore } from 'vuex'

export default createStore({
    state: {
        cnt: 1,
        price: 1000,
        status: 'none'
    },
    getters: {
        cnt: state => state.cnt,
        price: state => state.price,
        status: state => state.status,
        total(state) {
            return state.cnt * state.price
        }
    },
    mutations: {
        setCnt(state, newCnt) {
            state.cnt = Math.max(1, newCnt)
        },
        setStatus(state, status) {
            state.status = status
        }
    },
    actions: {
        increase(store) {
            store.commit('setCnt', store.state.cnt + 1)
        },
        decrease({ commit, state }) {
            commit('setCnt', state.cnt - 1)
        },
        setCnt(store, newCnt) {
            let cnt = parseInt(newCnt);
            store.commit('setCnt', isNaN(cnt) ? 1 : cnt)
        },
        sendOrder(store) {
            store.commit('setStatus', 'pending');
            setTimeout(() => {
                store.commit('setStatus', 'done');
            }, 1000)
        }
    },
    strict: process.env.NODE_ENV !== 'production',
})